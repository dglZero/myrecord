package com.dglZero.example.module.dao;

import com.dglZero.example.module.model.test2;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface testDao {

    void saveOrUpdate(List<test2> list);

}
