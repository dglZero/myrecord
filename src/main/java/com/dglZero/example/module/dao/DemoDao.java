package com.dglZero.example.module.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dglZero.example.module.model.Demo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface DemoDao extends BaseMapper<Demo> {
}
