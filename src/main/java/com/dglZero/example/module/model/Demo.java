package com.dglZero.example.module.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;

@TableName("test2")
public class Demo implements Serializable {
    @TableId
    @Excel(name = "id")
    private int id;
    @Excel(name="名字",orderNum = "1")
    private String name;
    @Excel(name = "内容",orderNum = "2")
    private String content;
    @TableField(exist = false)
    @Excel(name = "时间",exportFormat = "yyyy-MM-dd HH:mm:ss",orderNum = "3")
    private Date date;
    @TableField(exist = false)
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Demo(){

    }

    public Demo(String name, String content, Date date) {
        this.name = name;
        this.content = content;
        this.date = date;
    }
    public Demo(String name, String content) {
        this.name = name;
        this.content = content;
    }
    @Override
    public String toString() {
        return "Demo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
