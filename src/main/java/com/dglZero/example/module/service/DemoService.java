package com.dglZero.example.module.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dglZero.example.module.model.Demo;

import java.util.List;

public interface DemoService {
     IPage<Demo> selectUserPage(IPage page, String state);
}
