package com.dglZero.example.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dglZero.example.module.dao.DemoDao;
import com.dglZero.example.module.model.Demo;
import com.dglZero.example.module.service.DemoService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DemoServiceImpl implements DemoService {
    @Autowired
    private DemoDao dao;

    @Override
    public IPage<Demo> selectUserPage(IPage page, String state) {
        QueryWrapper ew = new QueryWrapper();
        ew.eq("id","1");
        return dao.selectPage(page,ew);
    }
}
