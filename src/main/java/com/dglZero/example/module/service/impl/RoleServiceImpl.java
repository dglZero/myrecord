/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dglZero.example.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dglZero.example.module.dao.RoleDao;
import com.dglZero.example.module.model.Role;
import com.dglZero.example.module.service.IRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 角色服务
 *
 * @author fengshuonan
 * @Date 2018/10/15 下午11:40
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements IRoleService {

    @Resource
    private RoleDao roleMapper;

    @Override
    public void setAuthority(Integer roleId, String ids) {

    }

    @Override
    public void delRoleById(Integer roleId) {

    }

    public List<Map<String, Object>> selectRoles(String condition) {
        return this.baseMapper.selectRoles(condition);
    }

    public int deleteRolesById(Integer roleId) {
        return this.baseMapper.deleteRolesById(roleId);
    }


}
