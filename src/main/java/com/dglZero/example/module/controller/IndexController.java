package com.dglZero.example.module.controller;

import com.dglZero.example.base.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/index")
public class IndexController {
    @Autowired
    private FileService fileService;
    @RequestMapping("/")
    public String index(HttpServletRequest request){
       // request.setAttribute("recordFiles",fileService.getFilePath());
        return "view/index";
    }

}
