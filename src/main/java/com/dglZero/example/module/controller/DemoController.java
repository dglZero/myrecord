package com.dglZero.example.module.controller;

import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dglZero.example.base.controller.GlobalControllerAdvice;
import com.dglZero.example.module.model.Demo;
import com.dglZero.example.module.service.impl.DemoServiceImpl;
import com.dglZero.example.utils.Downloads;
import com.dglZero.example.utils.Excel;
import com.dglZero.example.utils.RedisUtils;
import com.dglZero.example.utils.phoneMsg;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *   ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 *    ┃　　　┃
 *    ┃　　　┃
 *    ┃　　　┗━━━┓
 *    ┃　　　　　　　┣┓
 *    ┃　　　　　　　┏┛
 *    ┗┓┓┏━┳┓┏┛
 *      ┃┫┫　┃┫┫
 *      ┗┻┛　┗┻┛
 *        神兽保佑
 *        代码无BUG!
 * 功能描述: 例子
 * @Type: demo
 * @param:
 * @return: 
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:38
 */


@Controller
@RequestMapping("/demo")
public class DemoController   {
    @Value("${uploadPath.file}")
    private String uploadPath;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private DemoServiceImpl demoServiceImpl;
    @RequestMapping("test2")
    @ResponseBody
    public JSONObject test2(){
        //JSONObject object = fileService.getFilePath();
        JSONObject object = new JSONObject();
        object.put("aa","ss");
        return object;

    }
    @RequestMapping("test")
    public String test(HttpServletRequest request) {
        return "view/test";
    }


    @RequestMapping("index")
    public String demo(HttpServletRequest request) {
        /*//要用com.baomidou.mybatisplus.plugins.Page;
        Page page=new Page(1,10);
        List<Demo> list = demoServiceImpl.selectUserPage(page, "NORMAL");
        request.setAttribute("list",list);*/
        return "view/index";
    }

    /**
     *
     * 功能描述: 导出excel文件
     * @Type:  exportExcel
     * @param: [response]
     * @return: void
     * @auther: 杜冈霖
     * @date: 2018/8/28 9:39
     */
    @RequestMapping("exportExcel")
    public void export(HttpServletResponse response){
        IPage page=new Page(1,10);
        IPage<Demo> list = demoServiceImpl.selectUserPage(page, "NORMAL");

        QueryWrapper<Demo> ew = new QueryWrapper<>();

        //导出操作
        Excel.exportExcel(list.getRecords(),"标题","Sheet名字",Demo.class,"ExcelDemo.xls",response);
    }

    /**
     *
     * 功能描述:导入excel文件
     * @Type: importExecl
     * @param: [file]
     * @return: java.lang.String
     * @auther: 杜冈霖
     * @date: 2018/8/23 18:13
     */
    @RequestMapping(value = "importExcel",method = RequestMethod.POST)
    @ResponseBody
    public String importExcel(MultipartFile excelFile){
        List<Demo> list = Excel.importExcel(excelFile,1,1,Demo.class);

        return "导入数据一共【"+list.size()+"】行";
    }

    /**
     *
     * 功能描述:上传文件
     * @Type: uploadFile
     * @param: [demo]
     * @return: void
     * @auther: 杜冈霖
     * @date: 2018/8/28 9:40
     */
    @ApiOperation(value="上传文件", notes="上传指定的文件")
    @ApiImplicitParam(name = "demo", value = "上传的文件", required = true, dataType = "Demo")
    @RequestMapping("uploadFile")
    @ResponseBody
    public void uploadFile(Demo demo){
        MultipartFile multipartFile = demo.getFile();
        String path = uploadPath+multipartFile.getOriginalFilename();
        try {
            multipartFile.transferTo(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * 功能描述:下载文件
     * @Type: downloadFile
     * @param: [demo]
     * @return: void
     * @auther: 杜冈霖
     * @date: 2018/8/29 17:11
     */
    @ApiOperation(value="下载文件", notes="下载文件测试")
    @RequestMapping("downloadFile")
    public void downloadFile(HttpServletResponse response){
        String path = uploadPath+"22.txt";
       // Downloads.download(path,response);
      /*  try {
            Downloads.downloadNet("http://59.37.80.17/big.softdl.360tpcdn.com/auto/20180809/103352_f0174fc9995d908b6d1268cd77350329.exe",response,"QQ");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/
        try {
            Downloads.downLoadOpen(path,response,true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * 功能描述:添加信息参数
     * @Type: ResponseBodyParam
     * @param: []
     * @return: java.util.List<com.dglZero.example.module.model.Demo>
     * @auther: 杜冈霖
     * @date: 2018/8/28 9:41
     */
    @ResponseBody
    @RequestMapping(value = {"ss"})
    public List<Demo> ss() {
        List<Demo> employeeList=new ArrayList<>();
        Demo employee=new Demo();
        employee.setName("黄飞鸿");

        employeeList.add(employee);
        GlobalControllerAdvice.code= "1";
        GlobalControllerAdvice.reason= "2";

        try{
            int a = 0 / 0;
        }catch (Exception e){

        }
        return employeeList;
    }


    @ResponseBody
    @RequestMapping( value = "status")
    public Demo statusTest(HttpServletRequest request) {
        System.out.println(request);
        int a = 0 / 0;
        return new Demo();
    }

    @ResponseBody
    @RequestMapping("redisTest")
    public String redisTest(){
        stringRedisTemplate.opsForValue().set("ss","23");
        return stringRedisTemplate.opsForValue().get("ss")+"         "+redisUtils.getMap("1");
    }

    @ResponseBody
    @RequestMapping("testRole")
    @RequiresRoles(value = {"user","admin"},logical = Logical.OR)
    public String testRole(){
        return "success";
    }


    /**
     *
     * 功能描述:发送信息
     * @Type: phoneMsg
     * @param: [phoneNum, request]
     * @return: java.lang.String
     * @auther: 杜冈霖
     * @date: 2018/8/28 9:37
     */
    @RequestMapping("sendPhoneVail")
    @ResponseBody
    public String sendPhoneVail(@RequestParam(value = "phoneNum") String phoneNum, HttpServletRequest request) throws ClientException {
        //发短信
        SendSmsResponse response = null;
        try {
            response = phoneMsg.sendSms(phoneNum, request);

            System.out.println("短信接口返回的数据----------------");
            System.out.println("Code=" + response.getCode());
            System.out.println("Message=" + response.getMessage());
            System.out.println("RequestId=" + response.getRequestId());
            System.out.println("BizId=" + response.getBizId());

            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //查明细
            if (response.getCode() != null && response.getCode().equals("OK")) {
                QuerySendDetailsResponse querySendDetailsResponse = null;
                querySendDetailsResponse = phoneMsg.querySendDetails(response.getBizId());
                System.out.println("短信明细查询接口返回数据----------------");
                System.out.println("Code=" + querySendDetailsResponse.getCode());
                System.out.println("Message=" + querySendDetailsResponse.getMessage());
                int i = 0;
                for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs()) {
                    System.out.println("SmsSendDetailDTO[" + i + "]:");
                    System.out.println("Content=" + smsSendDetailDTO.getContent());
                    System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
                    System.out.println("OutId=" + smsSendDetailDTO.getOutId());
                    System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
                    System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
                    System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
                    System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
                    System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
                }
                System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
                System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
            }
            return "发送成功";
        } catch (ClientException e) {
            e.printStackTrace();
            return "发送失败";
        }
    }

}
