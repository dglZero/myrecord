package com.dglZero.example.module.controller;

import com.dglZero.example.base.model.CodeMsgHelper;
import com.dglZero.example.config.redis.ShiroRedisCache;
import com.dglZero.example.config.shiro.CredentialsMatcher;
import com.dglZero.example.config.shiro.ShiroKit;
import com.dglZero.example.config.shiro.service.UserAuthService;
import com.dglZero.example.module.model.ShiroUser;
import com.dglZero.example.module.model.User;
import com.dglZero.example.module.service.IUserService;
import com.dglZero.example.utils.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.*;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private IUserService userService;
    @Autowired
    private RedisTemplate redisTemplate;
    ShiroRedisCache redisCache = new ShiroRedisCache(redisTemplate);
    @Autowired
    private UserAuthService userAuthService;

    @RequestMapping("/")
    public String login(HttpServletRequest request) {
        return "view/login";
    }

    @RequestMapping("/test2")
    public String test2(HttpServletRequest request) {
        return "view/test2";
    }

    /**
     *
     * 功能描述:登录
     * @Type: login
     * @param: [request, session]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @auther: 杜冈霖
     * @date: 2018/9/17 11:37
     */
    @RequestMapping("loginUser")
    @ResponseBody
    public Map<String,Object> loginUser(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
       /* Cookie[] cookie = request.getCookies();
        for (Cookie c:  cookie) {
            System.out.println(c.getValue());
        }*/
        System.out.println(request.getSession().getId());
        String error = null;
        String ip = null;
        try {
           ip = Net.getIpAddr(request);
        } catch (Exception e) {
            error = "网络错误";
        }
        CredentialsMatcher.ip = ip;
        Map<String,Object> map = new HashMap<>();
        Map<String,String> mapParam = Parameter.setMap(request);
        String username = mapParam.get("username");
        String password = mapParam.get("password");
        String code = mapParam.get("code").toLowerCase();
        if(session.getAttribute("VerifyCode")==null){
            map.put("pass",false);
            map.put("reload",true);
            return map;
        }
        String VerifyCode = session.getAttribute("VerifyCode").toString().replace(" ","").toLowerCase();

        //判断验证码
        if(!VerifyCode.equals(code)){
            return CodeMsgHelper.setErrorlMsg("验证码不正确");
        }
        //设置token
        UsernamePasswordToken usernamePasswordToken=new UsernamePasswordToken(username,password,false);
        Subject subject = SecurityUtils.getSubject();

        try {
            subject.login(usernamePasswordToken);   //完成登录
          //  ShiroUser shiroUser = ShiroKit.getUser();
            //JSONObject jsonObject = JSONObject.fromObject(shiroUser);
            //map.put("shiroUser",jsonObject.toString());
           // JSONObject object = JSONObject.fromObject(map);


            //stringRedisTemplate.opsForHash().putAll(ip,object);
            //session.setAttribute("shiroUser",shiroUser);

            return CodeMsgHelper.setSuccessfulMsg("登录成功");
        } catch (UnknownAccountException e) {
            error = "用户名/密码错误";
        } catch (IncorrectCredentialsException e) {
            error = "用户名/密码错误";
        } catch (ExcessiveAttemptsException e) {
            // TODO: handle exception
            error = "登录失败多次，锁定5秒";
        } catch (AuthenticationException e) {
            // 其他错误，比如锁定，如果想单独处理请单独catch处理
            error = "其他错误：" + e.getMessage();
        }finally {
            int errcount = (int)redisTemplate.opsForValue().get(redisCache.getBytesKey(ip));
            if(errcount!=0)map.put("remainCount",(5-errcount));
        }
        return CodeMsgHelper.setErrorlMsg(error);


    }
    /**
     *
     * 功能描述: 登出
     * @Type:
     * @param: [session]
     * @return: java.lang.String
     * @auther: 杜冈霖
     * @date: 2018/9/17 11:37
     */
    @RequestMapping("logOut")
    @ResponseBody
    public boolean logOut(HttpSession session) {
        Subject subject = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser)subject.getPrincipal();
        String account = shiroUser.getAccount();

        subject.logout();
        ShiroRedisCache redisCache = new ShiroRedisCache(redisTemplate);

        Deque<Serializable> deque =  (Deque<Serializable>)redisCache.get(account);
        if(deque==null){
            return true;
        }
        deque.remove(session.getId());

        redisCache.put(account,deque);
//        session.removeAttribute("user");
        return false;
    }

    /**
     *
     * 功能描述: 注册
     * @Type:
     * @param: [request, session]
     * @return: java.lang.String
     * @auther: 杜冈霖
     * @date: 2018/9/17 11:37
     */
    @RequestMapping("registerUser")
    @ResponseBody
    public Map<String,Object> registerUser(HttpServletRequest request,HttpSession session){
        Map<String,String> map = Parameter.setMap(request);
        Map<String,Object> map2 = new HashMap<>();
        String username = map.get("username");

        User userChecked = userService.getByAccount(username);
        if(userChecked!=null){
            return CodeMsgHelper.setErrorlMsg("已存在相同名字");
        }
       /* Object mailCheck = session.getAttribute(username+"EmailCode");
        if(mailCheck==null){
            return CodeMsgHelper.setErrorlMsg("请重新获取邮箱验证码");
        }
        String updateMail = map.get("EmailCode");
        if(!updateMail.equals(mailCheck)){
            return CodeMsgHelper.setErrorlMsg("邮箱验证码错误");
        }*/
        String password = map.get("password");
        String salt = ShiroKit.getRandomSalt(5);
        String encodedPassword = ShiroKit.md5(password,salt);
        //设置注册资料
        User user = new User();
        user.setName(username);
        user.setAccount(username);
        user.setPassword(encodedPassword);
        user.setRoleid("5");
        user.setSalt(salt);
        user.setStatus(1);
      //  user.setEmail(map.get("Email"));

            try{
                userService.addUser(user);

              /*  Map<String,String> userRole = new HashMap<>();
                userRole.put("uid",user.getId().toString());
                userRole.put("rid","1");//暂时先默认*/
            }catch (Exception e){
                return CodeMsgHelper.setErrorlMsg("注册错误");

            }

        return CodeMsgHelper.setSuccessfulMsg("注册成功");
    }

    @RequestMapping("sendEmail")
    @ResponseBody
    public boolean sendEmail(HttpSession session,String username,String getEmail){
        Random random = new Random();
        String code = "123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        String msg="";
        for (int i = 0; i < 4; i++) {
            String rand = String.valueOf(code.charAt(random.nextInt(code.length())));
            msg+=rand;
        }
        SendEmail email = new SendEmail();
        boolean sendStatus = false;

        try{
            email.send(getEmail,"验证码",msg);
            sendStatus=true;
        }catch (Exception e){
            sendStatus = false;
        }


        if(!sendStatus){
            return false;
        }
        session.setAttribute(username+"EmailCode",msg);
        return true;


    }


}