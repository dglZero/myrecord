package com.dglZero.example.base.utils;

/**
 * 功能描述:返回值添加参数
 * @Type: ResponseBodyParam
 * @param:
 * @return:
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:27
 */
public final class StatusInfoUtils {

    //返回状态信息
    public static final String SUCCESSCODE="SUCCESS";
    public static final String SUCCESSMSG="操作成功";
    public static final String FAILCODE="FAIL";
    public static final String FAILMSG="操作失败";

}
