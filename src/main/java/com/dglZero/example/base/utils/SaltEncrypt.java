package com.dglZero.example.base.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class SaltEncrypt {

    public static String encrypt(Object username,Object inPassword){
        Object salt1 = ByteSource.Util.bytes(username);
        Object salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
        int hashIterations = 3;
        SimpleHash hash = new SimpleHash("md5", inPassword,
                salt1, hashIterations);
        return hash.toHex();
    }
}
