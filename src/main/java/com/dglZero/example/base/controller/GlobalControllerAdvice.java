package com.dglZero.example.base.controller;

import com.dglZero.example.base.model.CodeMsgHelper;
import com.dglZero.example.base.port.StatusInterface;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

import java.lang.annotation.Annotation;

/**
 * 功能描述: 异步请求返回参数（未完成）
 * @Type: ResponseBodyParam
 * @param: 
 * @return: 
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:27
 */
@RestControllerAdvice
public class GlobalControllerAdvice/* extends AbstractMappingJacksonResponseBodyAdvice */ {
    public static String code = "";
    public static String value;
    public static String reason = "";


    /**
     * 后处理，在方法有RestWrapper注解时，包装return type
     */
   /* @Override*/
   /* protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer,
                                           MediaType contentType, MethodParameter returnType,
                                           ServerHttpRequest request, ServerHttpResponse response) {
        if (returnType.getMethod().isAnnotationPresent(StatusInterface.class)) {
            bodyContainer.setValue(getWrapperResponse(returnType,request,
                    bodyContainer.getValue()));
        }
    }*/

   /* private CodeMsgHelper getWrapperResponse(MethodParameter returnType,
                                        ServerHttpRequest req,
                                        Object content) {
        return new CodeMsgHelper(code, reason, content);
    }*/

}
