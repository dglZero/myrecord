package com.dglZero.example.base.controller;

import com.dglZero.example.base.service.FileService;
import com.dglZero.example.utils.Downloads;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    private FileService fileService;
    @Value("${uploadPath.file}")
    private String path;


    @RequestMapping("download")
    public void download(HttpServletRequest request, HttpServletResponse response,String leftPath){

        Downloads.download(path+leftPath,response);

    }

    @RequestMapping("getMyrecordFiles")
    public JSONArray getMyrecordFiles(){
        JSONArray array = fileService.getFilePath();
        return array;

    }


}
