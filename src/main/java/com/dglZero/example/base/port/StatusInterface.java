package com.dglZero.example.base.port;


import com.dglZero.example.base.utils.StatusInfoUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述:返回值添加参数
 * @Type: ResponseBodyParam
 * @param:
 * @return:
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:27
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface StatusInterface {

    String code() default StatusInfoUtils.SUCCESSCODE;
    String msg() default StatusInfoUtils.SUCCESSMSG;

}
