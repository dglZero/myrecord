package com.dglZero.example.base.service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class FileService {
    @Value("${uploadPath.file}")
    private String FilePath;
    private int index = 1;

    public JSONArray getFilePath() {
        File file = new File(FilePath);
        return getFiles2(file);
    }

    private JSONObject getFiles(File files) {
        JSONObject object = new JSONObject();
        File[] listFiles = files.listFiles();
        for (int i = 0; i < listFiles.length; i++) {
            if (listFiles[i].isFile()) {
                object.put(listFiles[i].getName(), listFiles[i].getPath());
            } else if (listFiles[i].isDirectory()) {
                JSONArray array = new JSONArray();
                array.add(getFiles(listFiles[i]));
                object.put(listFiles[i].getName(), array);

            }
        }
        return object;
    }

    private JSONArray getFiles2(File files) {

        File[] listFiles = files.listFiles();
        JSONArray array = new JSONArray();
        for (int i = 0; i < listFiles.length; i++) {
            JSONObject object = new JSONObject();
            //System.out.println("文     件："+tempList[i]);
            object.put("name", listFiles[i].getName());
            object.put("value", index);
          /*  JSONObject object2= new JSONObject();
            object2.put("borderColor","green");
            object.put("itemStyle",object2);*/

            if (listFiles[i].isDirectory()) {
                // System.out.println("文件夹："+tempList[i]);
                JSONArray array2 = getFiles2(listFiles[i]);
                object.put("children", array2);
                if (array2.size() == 0) {
                    continue;
                }
            }

            array.add(object);
        }
        return array;
    }
}
