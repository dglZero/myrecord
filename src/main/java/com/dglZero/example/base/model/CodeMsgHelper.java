package com.dglZero.example.base.model;


import java.util.HashMap;
import java.util.Map;

/**
 *
 * 功能描述:返回值添加参数
 * @Type: ResponseBodyParam
 * @param:
 * @return:
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:27
 */
public class CodeMsgHelper {

    private  String code;
    private  boolean pass;
    private  String msg;

    public static Map<String,Object>  setSuccessfulMsg(String msg){
        Map<String,Object> map = new HashMap<>();
        map.put("pass",true);
        map.put("msg",msg);
        return map;
    }
    public static Map<String,Object>  setErrorlMsg(String msg){
        Map<String,Object> map = new HashMap<>();
        map.put("pass",false);
        map.put("msg",msg);
        return map;
    }
    public static Map<String,Object>  setSuccessfulMsg(String msg,String code){
        Map<String,Object> map = new HashMap<>();
        map.put("pass",true);
        map.put("msg",msg);
        map.put("code",code);
        return map;
    }
    public static Map<String,Object>  setErrorlMsg(String msg,String code){
        Map<String,Object> map = new HashMap<>();
        map.put("pass",false);
        map.put("msg",msg);
        map.put("code",code);
        return map;
    }
    public CodeMsgHelper(boolean pass,String msg,String code){
        this.pass=pass;
        this.msg=msg;
        this.code=code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isPass() {
        return pass;
    }

    public void setPass(boolean pass) {
        this.pass = pass;
    }
}
