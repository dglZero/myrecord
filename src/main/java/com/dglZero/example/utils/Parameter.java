package com.dglZero.example.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

//设置传递参数URL格式
public class Parameter {

	// 设置查询参数
	public static Map<String, Object> setMap(HttpServletRequest request,String formeky) {
		Enumeration<String> names = request.getParameterNames();// 获取
		// 前端页面传过来的所有参数key
		// 创建存放条件的map集合
		Map<String, Object> map = getSessionParameter(request,formeky);
		while (names.hasMoreElements()) {// 遍历所有key
			String string = (String) names.nextElement();// 获取key
			map.put(string, request.getParameter(string));// 通过key 获取key值 放入map

		}
		map.put("formeky", formeky);
		return map;
	}
	// 设置查询参数
	public static Map<String, String> setMap(HttpServletRequest request) {
		Enumeration<String> names = request.getParameterNames();// 获取
		// 前端页面传过来的所有参数key
		// 创建存放条件的map集合
		Map<String, String> map = new HashMap<>();
		while (names.hasMoreElements()) {// 遍历所有key
			String string = (String) names.nextElement();// 获取key
			map.put(string, request.getParameter(string));// 通过key 获取key值 放入map
		}
		return map;
	}

	// 设置参数 放入一次请求作用域
	public static void setRquest(HttpServletRequest request) {
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String string = (String) names.nextElement();
			request.setAttribute(string, request.getParameter(string));// 放入作用域
		}
	}

	// 设置参数 放入一次请求作用域
	public static void setMapRquest(HttpServletRequest request,Map<String, Object> map) {
		for (Entry<String, Object> entry : map.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());// 放入作用域
		}
	}

	// 获取 封装的参数
	public static Map<String, Object> getSessionParameter(HttpServletRequest request,String formkey) {
		// 创建存放条件的map集合
		Map<String, Object> map = new HashMap<String, Object>();
		Object parms = request.getSession().getAttribute("Parameter");
		if (parms == null) {
			return map;
		}
		String[] str = parms.toString().split("&");
		for (String string : str) {
			String[] parm = string.split("=");
			if (parm.length <= 1) {
				continue;
			}
			//条件符合 代表条件不是本表单的
			if(parm[0].equals("formeky") && !parm[1].equals(formkey)){
				//清空map
				map.clear();
				break;
			}
			//存在分页参数在 session作用域删除它
			if(parm[0].contains("bpmDataTemplate"))continue;
			map.put(parm[0], parm[1]);
		}
		return map;
	}

	// 设置传送参数格式：delParam=a,b,c&e=val&f=val
	public static void setParameter(HttpServletRequest request, Map<String, Object> map) {
		String str = "";
		String[] varDel;
		if (map.containsKey("delParam")) {//指定删除
			varDel = map.get("delParam").toString().split(",");
			for (String string : varDel) {
				map.remove(string);
			}
			map.remove("delParam");//删除特定Key
		}
		for (Entry<String, Object> entry : map.entrySet()) {
			str += entry.getKey() + "=" + entry.getValue() + "&";
		}
		str = str.equals("") ? "" : str.substring(0, str.length() - 1);
		request.getSession().setAttribute("Parameter", str);
	}

	// 添加传送参数
	public static void addParameter(HttpServletRequest request, String... objs) {

		String parameter = request.getParameter("Parameter");
		for (String string : objs) {// 添加参数
			parameter += "&" + string;
		}
		request.getSession().setAttribute("Parameter", parameter);

	}




	// 方法二：通过类加载目录getClassLoader()加载属性文件    
	public static String getPropertyByName2(Class tClass,String path, String name) {
		String result = "";    

		// 方法二：通过类加载目录getClassLoader()加载属性文件    
		InputStream in = tClass.getClassLoader().getResourceAsStream(path);
		// InputStream in =    
		// this.getClass().getClassLoader().getResourceAsStream("mailServer.properties");    

		// 注：Object.class.getResourceAsStream在action中调用报错，在普通java工程中可用    
		// InputStream in =    
		// Object.class.getResourceAsStream("/mailServer.properties");    
		Properties prop = new Properties();    
		try {    
			prop.load(in);    
			result = prop.getProperty(name).trim();    
		} catch (IOException e) {    
			System.out.println("读取配置文件出错");    
			e.printStackTrace();    
		}    
		return result;    
	} 
}
