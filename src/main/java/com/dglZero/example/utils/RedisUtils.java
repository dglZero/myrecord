package com.dglZero.example.utils;

import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 功能描述: Redis工具类
 * @Type:
 * @param: 
 * @return: 
 * @auther: 杜冈霖
 * @date: 2018/9/4 17:52
 */
@Service
public class RedisUtils {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置map
     * @param key
     * @param map
     */
    public void setMap(String key, Map<String,Object> map){
        JSONObject object = JSONObject.fromObject(map);
        stringRedisTemplate.opsForHash().putAll(key,object);
    }

    /**
     * 获取map
     * @param key
     * @return
     */
    public Map getMap(String key){
        Map<String,String> map = new HashMap<>();
        Cursor<Map.Entry<Object, Object>> curosr = stringRedisTemplate.opsForHash().scan(key, ScanOptions.NONE);
        while(curosr.hasNext()){
            Map.Entry<Object, Object> entry = curosr.next();
            map.put(entry.getKey().toString(),entry.getValue().toString());
        }
        return map;
    }


}
