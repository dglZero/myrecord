package com.dglZero.example.config.redis;

import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

/**
 *
 * 功能描述: 
 * @Type: Redis
 * @param:
 * @return: 
 * @auther: 杜冈霖
 * @date: 2018/9/4 1:21
 */
@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisConfig {
    @Autowired
    private RedisProperties redisProperties;

    /**
     * redis序列化工具
     *
     * @return
     */
    @Bean
    public RedisSerializer<Object> genericJackson2JsonRedisSerializer() {
        return new GenericJackson2JsonRedisSerializer();
    }

    /**
     * 创建redisTemplate
     *
     * @return
     */
    @Bean
    @Primary
    public RedisTemplate<Object, Object> functionDomainRedisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setHashKeySerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setValueSerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setHashValueSerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setConnectionFactory(functionDomainRedisConnectionFactory());
//        redisTemplate.setDefaultSerializer(genericJackson2JsonRedisSerializer());
//        redisTemplate.setEnableDefaultSerializer(true);
        return redisTemplate;
    }

    /**
     * redis操作用connectionfactory
     *
     * @return
     */
    @Bean
    @Primary
    public RedisConnectionFactory functionDomainRedisConnectionFactory() {
        RedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(jedisPoolConfig());
        ((JedisConnectionFactory) redisConnectionFactory).setHostName(this.redisProperties.getHost());
        ((JedisConnectionFactory) redisConnectionFactory).setPassword(this.redisProperties.getPassword());
        ((JedisConnectionFactory) redisConnectionFactory).setPort(this.redisProperties.getPort());
        ((JedisConnectionFactory) redisConnectionFactory).setTimeout(this.redisProperties.getTimeout());
        ((JedisConnectionFactory) redisConnectionFactory).setDatabase(9);
        return redisConnectionFactory;
    }

    /**
     * 创建缓存用connectionfactory
     *
     * @return
     */
    @Bean
    public RedisConnectionFactory cacheRedisConnectionFactory() {
        RedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(jedisPoolConfig());
        ((JedisConnectionFactory) redisConnectionFactory).setHostName(this.redisProperties.getHost());
        ((JedisConnectionFactory) redisConnectionFactory).setPassword(this.redisProperties.getPassword());
        ((JedisConnectionFactory) redisConnectionFactory).setPort(this.redisProperties.getPort());
        ((JedisConnectionFactory) redisConnectionFactory).setTimeout(this.redisProperties.getTimeout());
        ((JedisConnectionFactory) redisConnectionFactory).setDatabase(6);
        return redisConnectionFactory;
    }

    /**
     * 创建缓存用redistemplate
     *
     * @return 缓存用redistemplae
     */
    @Bean
    public RedisTemplate<Object, Object> cacheRedisTemplate() {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setHashKeySerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setValueSerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setHashValueSerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setConnectionFactory(cacheRedisConnectionFactory());
        redisTemplate.setDefaultSerializer(genericJackson2JsonRedisSerializer());
        redisTemplate.setEnableDefaultSerializer(true);
        return redisTemplate;
    }

    /**
     * 创建jedispoolconfig
     *
     * @return
     */
    @Bean
    public JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        RedisProperties.Pool props = this.redisProperties.getPool();
        jedisPoolConfig.setMaxTotal(props.getMaxActive());
        jedisPoolConfig.setMaxIdle(props.getMaxIdle());
        jedisPoolConfig.setMinIdle(props.getMinIdle());
        jedisPoolConfig.setMaxWaitMillis(props.getMaxWait());
        return jedisPoolConfig;
    }

    /**
     * 创建缓存管理器bean
     *
     * @return cachemanager
     */
    @Bean
    public CacheManager redisCacheManager(JedisPoolConfig jedisPoolConfig) {
        RedisCacheManager cacheManager = new RedisCacheManager(cacheRedisTemplate());
        cacheManager.setDefaultExpiration(60);
        cacheManager.setLoadRemoteCachesOnStartup(true);
        cacheManager.setUsePrefix(true);
        System.err.println("--------------redis已经加载----------------");
        return cacheManager;
    }

    /**
     * reids消息监听器
     *
     * @return
     */
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(functionDomainRedisConnectionFactory());
        return container;
    }

    /**
     * 缓存过期时间监听
     *
     * @return
     */
    public RedisMessageListenerContainer cacheExpireMessageListenerContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(cacheRedisConnectionFactory());
        return container;
    }





}