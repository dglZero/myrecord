package com.dglZero.example.config;

import com.dglZero.example.MyrecordApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述: springboot部署
 * @Type: springboot
 * @auther: 杜冈霖
 * @date: 2018/11/25 20:07
 */
@Configuration
public class springbootConfig extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MyrecordApplication.class);
    }
}
