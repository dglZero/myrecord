package com.dglZero.example.config.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.dglZero.example.config.redis.RedisSessionConfig;
import com.dglZero.example.config.redis.RedisSessionDao;
import com.dglZero.example.config.redis.ShiroRedisCacheManager;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.Filter;
import java.util.LinkedHashMap;

/**
 *
 * 功能描述: shiro配置以及权限拦截
 * @Type: shiro
 * @param:
 * @return:
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:31
 */
@Configuration
@PropertySource({"classpath:path.properties"})
public class ShiroConfiguration {
    @Value("${PermissionPath.path}")
    private String permissPath;
    @Autowired
    private RedisTemplate redisTemplate;

    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(@Qualifier("securityManager") SecurityManager manager,SessionManager sessionManager) {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(manager);
        //配置登录的url
        bean.setLoginUrl("/login/");
        //配置登陆成功后跳转页面
        bean.setSuccessUrl("/view/index");
        //配置权限不足时跳转的页面
        bean.setUnauthorizedUrl("/view/403");
        //限制同一帐号同时在线的个数
        //自定义拦截器限制并发人数
        LinkedHashMap<String, Filter> filtersMap = new LinkedHashMap<>();
        filtersMap.put("kickout", kickoutSessionControlFilter(sessionManager,redisCacheManager(redisTemplate)));
        //统计登录人数
        bean.setFilters(filtersMap);
        //配置访问权限
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        String[] paths = permissPath.split(",");
        for (String path : paths) { //表示可以匿名访问
            filterChainDefinitionMap.put(path, "anon");
        }
        filterChainDefinitionMap.put("/**", "kickout,user");//表示需要认证才可以访问
        bean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return bean;
    }

    /**
     * 配置核心安全事务管理器
     *
     * @param authRealm
     * @return
     */
    @Bean(name = "securityManager")
    public SecurityManager securityManager(@Qualifier("authRealm") AuthRealm authRealm,ShiroRedisCacheManager shiroRedisCacheManager) {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(authRealm);
        //配置记住我
        manager.setRememberMeManager(rememberMeManager());
        //配置redis缓存
       manager.setCacheManager(shiroRedisCacheManager);
        //配置自定义session管理，使用redis
        //manager.setSessionManager(RedisSessionConfig);
        System.err.println("--------------shiro已经加载----------------");
        return manager;
    }

    /**
     * 配置自定义的权限登录器
     *
     * @param matcher
     * @return
     */
    @Bean(name = "authRealm")
    public AuthRealm authRealm(@Qualifier("credentialsMatcher") CredentialsMatcher matcher) {
        AuthRealm authRealm = new AuthRealm();
        authRealm.setCredentialsMatcher(matcher);
        return authRealm;
    }

    /**
     * 配置自定义的密码比较器
     *
     * @return
     */
    @Bean(name = "credentialsMatcher")
    public CredentialsMatcher credentialsMatcher(RedisTemplate redisTemplate) {
        return new CredentialsMatcher(redisTemplate);
    }

    /**
     * shiro管理生命周期的东西
     *
     * @return
     */
    @Bean
    public static LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator creator = new DefaultAdvisorAutoProxyCreator();
        creator.setProxyTargetClass(true);
        return creator;
    }

    /**
     * 开启shiro注解模式
     * @param manager
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(@Qualifier("securityManager") SecurityManager manager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(manager);
        return advisor;
    }

    @Bean(name = "AuthRealm")
    @DependsOn(value = {"lifecycleBeanPostProcessor", "ShiroRedisCacheManager"})
    public AuthRealm myShiroRealm(RedisTemplate redisTemplate) {
        AuthRealm shiroRealm = new AuthRealm();
        //设置缓存管理器
        shiroRealm.setCacheManager(redisCacheManager(redisTemplate));
        shiroRealm.setCachingEnabled(true);
        //设置认证密码算法及迭代复杂度
        shiroRealm.setCredentialsMatcher(credentialsMatcher(redisTemplate));
        //认证
        shiroRealm.setAuthenticationCachingEnabled(false);
        //授权
        shiroRealm.setAuthorizationCachingEnabled(false);
        return shiroRealm;
    }

    /**
     * 缓存管理器的配置
     *
     * @param redisTemplate
     * @return
     */
    @Bean(name = "ShiroRedisCacheManager")
    public ShiroRedisCacheManager redisCacheManager(RedisTemplate redisTemplate) {
        ShiroRedisCacheManager redisCacheManager = new ShiroRedisCacheManager(redisTemplate);
        //name是key的前缀，可以设置任何值，无影响，可以设置带项目特色的值
        redisCacheManager.createCache("shiro_redis");
        return redisCacheManager;
    }

    /**
     * cookie管理对象;记住我功能
     */
    @Bean
    public CookieRememberMeManager rememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie());
        //rememberMe cookie加密的密钥 建议每个项目都不一样 默认AES算法 密钥长度(128 256 512 位)
        cookieRememberMeManager.setCipherKey(Base64.decode("3AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }

    public SimpleCookie rememberMeCookie() {
        SimpleCookie simpleCookie = new SimpleCookie("remenbermeCookie");
        //<!-- 记住我cookie生效时间 ,单位秒;-->
        simpleCookie.setMaxAge(3600);
        return simpleCookie;
    }

    /**
     * FormAuthenticationFilter 过滤器 过滤记住我
     * @return
     */
    @Bean
    public FormAuthenticationFilter formAuthenticationFilter(){
        FormAuthenticationFilter formAuthenticationFilter = new FormAuthenticationFilter();
        //对应前端的checkbox的name = rememberMe
        formAuthenticationFilter.setRememberMeParam("rememberMe");
        return formAuthenticationFilter;
    }

    /**
     * 并发登录控制
     * @return
     */
    @Bean
    public KickoutSessionControlFilter kickoutSessionControlFilter(SessionManager sessionManager,ShiroRedisCacheManager shiroRedisCacheManager){
       KickoutSessionControlFilter kickoutSessionControlFilter = new KickoutSessionControlFilter();
       //用于根据会话ID，获取会话进行踢出操作的；
       kickoutSessionControlFilter.setSessionManager(sessionManager);
       //使用cacheManager获取相应的cache来缓存用户登录的会话；用于保存用户—会话之间的关系的；
       kickoutSessionControlFilter.setCacheManager(shiroRedisCacheManager);
       //是否踢出后来登录的，默认是false；即后者登录的用户踢出前者登录的用户；
       kickoutSessionControlFilter.setKickoutAfter(false);
       //同一个用户最大的会话数，默认1；比如2的意思是同一个用户允许最多同时两个人登录；
       kickoutSessionControlFilter.setMaxSession(1);
       //被踢出后重定向到的地址；
       kickoutSessionControlFilter.setKickoutUrl("/login/");
       return kickoutSessionControlFilter;
   }



    /**
     *  配置sessionmanager，由redis存储数据
     */
  /*  @Bean(name = "sessionManager")
    @DependsOn(value = "lifecycleBeanPostProcessor")
    public DefaultWebSessionManager sessionManager(RedisTemplate redisTemplate) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        RedisSessionDao redisSessionDao = new RedisSessionDao(redisTemplate);
        //这个name的作用也不大，只是有特色的cookie的名称。
        redisSessionDao.setSessionIdGenerator(sessionIdGenerator());
        sessionManager.setSessionDAO(redisSessionDao);
        sessionManager.setDeleteInvalidSessions(true);
        SimpleCookie cookie = new SimpleCookie();
        cookie.setName("starrkCookie");
        sessionManager.setSessionIdCookie(cookie);
        sessionManager.setSessionIdCookieEnabled(true);
        //是否开启删除无效的session对象  默认为true
        sessionManager.setDeleteInvalidSessions(true);
        //取消url 后面的 JSESSIONID
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }*/

    /**
     * 自定义的SessionId生成器
     * @return
     */
    public SessionIdGenerator sessionIdGenerator() {
        return new JavaUuidSessionIdGenerator();
    }

    /***
     * 加入thymeleaf需要
     * @return
     */
    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }
}