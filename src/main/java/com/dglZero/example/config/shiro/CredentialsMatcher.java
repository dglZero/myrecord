package com.dglZero.example.config.shiro;

import com.dglZero.example.base.utils.HttpContext;
import com.dglZero.example.config.redis.ShiroRedisCache;
import com.dglZero.example.config.shiro.service.UserAuthService;
import com.dglZero.example.module.model.User;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 *
 * 功能描述: shiro密码对比
 * @Type: shiro
 * @param:
 * @return:
 * @auther: 杜冈霖
 * @date: 2018/8/28 9:31
 */
public class CredentialsMatcher extends HashedCredentialsMatcher {

    private RedisTemplate redisTemplate;
    public static String ip;
    @Autowired
    private UserAuthService userAuthService;

    public CredentialsMatcher(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {

        //获得用户输入的密码:(可以采用加盐(salt)的方式去检验)
        UsernamePasswordToken utoken = (UsernamePasswordToken) token;
        String inPassword = new String(utoken.getPassword());
        //获得数据库中的信息
        String dbPassword = (String) info.getCredentials();
        String account = (String) token.getPrincipal();
        User user = userAuthService.user(account);
        //编译用户输入密码
        String encodedPassword=ShiroKit.md5(inPassword, user.getSalt());

        //重登限制次数
        Long leaveTime = 10L;//默认10秒
        int retryCount = 0;
        ShiroRedisCache redisCache = new ShiroRedisCache(redisTemplate);

        if( redisCache.get(ip)!=null){
            retryCount = (int) redisCache.get(ip);
            leaveTime = redisTemplate.getExpire(ip);//获取当前剩余时间，重新赋值和判断
            if(leaveTime==-1){//判断过期，重设
                redisCache.put(ip,0);
            }
        }
        if (++retryCount> 4) {//自增
            byte[] bytes = redisCache.getBytesKey(ip);
            redisTemplate.expire(bytes, leaveTime, TimeUnit.MINUTES);//设置过期时间
            // 5次抛出
            throw new ExcessiveAttemptsException();
        }
        boolean matches = this.equals(encodedPassword, dbPassword);
        if (matches) {
            //清除次数
           redisCache.put(ip,0);
            HttpServletRequest request = HttpContext.getRequest();
            HttpSession session =  request.getSession();
            Deque<Serializable> deque =  (Deque<Serializable>)redisCache.get(account);
            if(deque==null){
                deque =new LinkedList<>();
            }
            deque.push(session.getId());

            redisCache.put(account,deque);
        } else {
            redisCache.put(ip, retryCount);
        }

        return matches;
    }

}